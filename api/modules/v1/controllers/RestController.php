<?php

namespace app\api\modules\v1\controllers;

use yii\rest\ActiveController;
use Yii;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use app\api\modules\v1\models\BookModel;
use yii\web\Controller;

class RestController extends Controller {

    public $modelClass = 'app\models\User';

    public function behaviors() {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];

        return $behaviors;
    }

    //to - do authenticate first
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    //get all books in database
    public function actionGet() {
        $book_m = new BookModel();
        $listofbooks = $book_m->getAllBooks();
        $this->setHeader(200);
        echo json_encode(array('status' => 1, 'data' => $listofbooks), JSON_PRETTY_PRINT);
    }

    //create new books
    public function actionCreate() {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $json = file_get_contents('php://input');
            parse_str($json, $output);
        } else {
            $this->setHeader(400);
            echo json_encode(array('status' => 0, 'error_code' => 400, 'message' => "Error creating user"), JSON_PRETTY_PRINT);
        }
        $params = array(
            'title' => $output['title'],
            'author' => $output['author'],
            'year' => $output['year'],
            'publisher' => $output['publisher']
        );

        $book_m = new BookModel();
        $createSuccess = $book_m->createBooks($params);
        if ($createSuccess == 1) {
            $this->setHeader(200);
            echo json_encode(array('status' => 1, 'data' => "Successfully created"), JSON_PRETTY_PRINT);
        } else {
            $this->setHeader(400);
            echo json_encode(array('status' => 0, 'error_code' => 400, 'message' => "Error creating user"), JSON_PRETTY_PRINT);
        }
    }

    //public function action remove one book
    public function actionRemove() {
        $book_m = new BookModel();
        $id = $_GET['id'];
        $book_m->deleteBook($id);
        $this->setHeader(200);
        echo json_encode(array('status' => 1, 'message' => 'Successfully Deleted'), JSON_PRETTY_PRINT);
    }

    //update a user
    public function actionUpdate($id) {
        $json = file_get_contents('php://input');
        parse_str($json, $output);
        $user_m = new BookModel();
        $update_succes = $user_m->updateBook($id, $output);
        if ($update_succes == 1) {
            $this->setHeader(200);
            echo json_encode(array('status' => 1, 'data' => "Successfully Updated with id=".$id), JSON_PRETTY_PRINT);
        } else {
            $this->setHeader(400);
            echo json_encode(array('status' => 0, 'error_code' => 400, 'message' => "Error updating user"), JSON_PRETTY_PRINT);
        }
    }

    //upload a xml file
    public function actionUpload() {
        ini_set('max_execution_time', 500);
        $target_dir = Yii::$app->basePath . "/uploads/";
        //  var_dump($_POST['file']);
        if (isset($_FILES['file'])) {
            $file_name = $_FILES['file']['name'];
            $file_size = $_FILES['file']['size'];
            $file_tmp = $_FILES['file']['tmp_name'];
            $file_type = $_FILES['file']['type'];

            $target_file = $target_dir . basename($file_name);
//            echo $target_file;
//            exit;
            $file_ext = strtolower(end(explode('.', $_FILES['file']['name'])));
            if ($file_ext !== 'xml') {
                $this->setHeader(400);
                echo json_encode(array('status' => 0, 'message' => 'File type not allowed'), JSON_PRETTY_PRINT);
            } else {
                if (move_uploaded_file($file_tmp, $target_file)) {
                    $this->setHeader(200);
                    echo json_encode(array('status' => 1, 'message' => 'Upload file successfully'), JSON_PRETTY_PRINT);
                } else {
                    $this->setHeader(400);
                    echo json_encode(array('status' => 0, 'message' => 'Error uploading file'), JSON_PRETTY_PRINT);
                }
            }
        } else {
            $this->setHeader(400);
            echo json_encode(array('status' => 0, 'message' => 'File not found!!'), JSON_PRETTY_PRINT);
        }
    }

    private function setHeader($status) {

        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        $content_type = "application/json; charset=utf-8";

        header($status_header);
        header('Content-type: ' . $content_type);
    }

    private function _getStatusCodeMessage($status) {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

}
