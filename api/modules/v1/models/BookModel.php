<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\api\modules\v1\models;

use Yii;
use yii\base\Model;

/**
 * Description of BookModel
 *
 * @author Rusiate Sevutaka
 */
class BookModel extends Model {

    //put your code here
    //put your code here
    var $db;
    var $command;

    public function init() {
        $this->db = Yii::$app->getDb();
        $this->command = $this->db->createCommand();
    }

    public function getAllBooks() {
        $command = $this->db->createCommand("SELECT * FROM book");
        $user_array = $command->queryAll();
        return $user_array;
    }

//    public function getUserById($userId) {
//        $command = $this->db->createCommand("SELECT * FROM user where id = {$userId}");
//        $user_array = $command->queryOne();
//        return $user_array;
//    }

    public function createBooks($params) {
        $command = "INSERT INTO book (title, author, year, publisher) VALUES ({$this->db->quoteValue($params['title'])}, {$this->db->quoteValue($params['author'])}, {$params['year']}, {$this->db->quoteValue($params['publisher'])})";
//        var_dump($command);
//        exit;
        try {
            if ($this->db->createCommand($command)->execute() == 1) {

                return 1;
            } else {
                return -1;
            }
        } catch (PDOException $e) {

            return -1;
        }
    }

    public function updateBook($id, $params) {
        $sql = "UPDATE `book` SET ";
        // Get array keys
        $arrayKeys = array_keys($params);
        // Fetch last array key
        $lastArrayKey = array_pop($arrayKeys);
        //iterate array
        foreach ($params AS $field_name => $field_value) {
            if ($field_name !== 'id') {
                if ($field_name == $lastArrayKey) {
                    $sql .= "`{$field_name}` = {$this->db->quoteValue($field_value)} ";
                } else {
                    $sql .= "`{$field_name}` = {$this->db->quoteValue($field_value)}, ";
                }
            }
        }
        $sql .= " WHERE `book`.id = {$this->db->quoteValue($id)} ";
//        var_dump($sql);
//        exit;
        try {
            $this->db->createCommand($sql)->execute();
            return 1;
        } catch (PDOException $e) {
            echo "DataBase Error: The book could not be added.<br>" . $e->getMessage();
            return -1;
        } catch (Exception $e) {
            echo "General Error: The book could not be added.<br>" . $e->getMessage();
            return -1;
        }
    }

    public function deleteBook($id) {
        $sql = "DELETE FROM book where id = {$this->db->quoteValue($id)}";
        $this->db->createCommand($sql)->execute();
        return 1;
    }

}
