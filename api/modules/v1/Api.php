<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Api
 *
 * @author Rusiate Sevutaka
 */

namespace app\api\modules\v1;
 
use \yii\base\Module;

class Api extends Module{
   public $controllerNamespace = 'app\api\modules\v1\controllers';
 
    public function init()
    {
        parent::init();
 
        // custom initialization code goes here
    }
}
