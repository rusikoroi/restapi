<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class TestController extends Controller {

    //local url for the api
    var $api_url = 'http://localhost/restApi/api/v1/rest';
    var $apiKey = '';
    var $access_token = '4p9mj82PTl1BWSya7bfpU_Nm8u07hkcB';

    public function actionIndex() {

        echo "This is test controller" . PHP_EOL;
        exit;
    }

    public function actionRest() {
        $userUrl = $this->api_url . '/';
        $supplier_info = $this->getResponseFromApi($userUrl, '', '', $this->apiKey, 'GET');
        $suppler_data = json_encode($supplier_info);
        var_dump($suppler_data);
        exit;
        return $suppler_data;
    }

    public function actionGet() {
        $userUrl = $this->api_url . '/' . 'get';
        $supplier_info = $this->getResponseFromApi($userUrl, 'GET');
        $suppler_data = json_encode($supplier_info);
        var_dump($suppler_data);
        exit;
        return $suppler_data;
    }

    public function actionCreate() {
        $params = http_build_query(
                array(
                    "title" => 'tielw',
                    "author" => 'test',
                    "year" => 122,
                    "publisher" => 'test'
                )
        );
        $userUrl = $this->api_url . '/' . 'create';
        $upload_result = $this->postDataToApi($userUrl, $params);
        var_dump($upload_result);
        exit;
    }

    public function actionRemove() {
        $id = 6;
        $userUrl = $this->api_url . '/' . 'remove?id=' . $id;
        $supplier_info = $this->getResponseFromApi($userUrl, 'GET');
        var_dump($supplier_info);
        exit;
    }

    public function actionUpdate() {
        $id = 7;
        $userUrl = $this->api_url . '/' . 'update?id=' . $id;
        $params = http_build_query(
                array(
                    "title" => 'book of mormon',
                    "author" => 'yehssye',
                    "year" => 122,
                    "publisher" => 'publisher'
                )
        );
        $upload_result = $this->postDataToApi($userUrl, $params);
        var_dump($upload_result);
        exit;
    }

    public function actionLoad() {
        echo 'loading';
        exit;
    }

    private function getResponseFromApi($url, $postType) {
        // $cftoken = base64_encode('Administrator:');
       
        $headers = array(
            'Authorization: Bearer ' . $this->access_token
        );
        $options = array(
            'http' => array(
                'header' => $headers,
                'timeout' => 180, // 600 Seconds = 10 Minutes
                'method' => $postType,
            ),
        );
        $context = stream_context_create($options);
        $json_str = @file_get_contents($url, false, $context);

        return($json_str);
    }

    public function postDataToApi($url, $postdata) {
       
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $this->access_token,
        );
        $options = array(
            'http' => array(
                'header' => $headers,
                'method' => 'POST',
                'content' => $postdata
            ),
        );
        $context = stream_context_create($options);
//        var_dump($context);
//        exit;
        $json_str = @file_get_contents($url, false, $context);
//        var_dump($http_response_header);
//        exit;
        return($json_str);
    }

}
